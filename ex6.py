

def is_prime(number):
    if number < 2:
        return 'Число не является простым и не является составным.'
    elif number == 2 or number == 3:
        return 'Простое'
    elif number % 2 == 0 or number % 3 == 0:
        return 'Составное'
    else:
        for divider in range(5, int(number ** 0.5) + 1, 6):
            if number % divider == 0 or number % (divider + 2) == 0:
                return 'Составное'
        return 'Простое'

number = int(input("Введите число: "))
print(is_prime(number))