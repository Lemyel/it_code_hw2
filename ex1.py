import datetime
import random


date = datetime.date.today()
months = {
            1:'января',
            2:'февраля',
            3:'марта',
            4:'апреля',
            5:'мая',
            6:'июня',
            7:'июля',
            8:'августа',
            9:'сентября',
            10:'октября',
            11:'ноября',
            12:'декабря'
        }

temperature = random.randint(-30, 30)
print(f"Сегодня {date.day} {months[date.month]}. На улице {temperature} градусов.")
if temperature <= 0:
    print("Холодно, лучше остаться дома")
